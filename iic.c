
/* This code is sued for starting continous ranging on SRF02 sensor
 *
 * (c) Red Pitaya  http://www.redpitaya.com
 *
 * This part of code is written in C programming language.
 * Please visit http://en.wikipedia.org/wiki/C_(programming_language)
 * for more details on the language used herein.
 */


#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <linux/ioctl.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>



#define I2C_SLAVE_FORCE                    0x0706
#define I2C_SLAVE                          0x0703    /* Change slave address            */
#define I2C_FUNCS                          0x0705    /* Get the adapter functionality */
#define I2C_RDWR                           0x0707    /* Combined R/W transfer (one stop only)*/



int main(int argc, char **argv)
{
        //printf("SRF02 Ranging program\n");

        int fd;                                                                         // File des$
        char *fileName = "/dev/i2c-0";                                                  // Name of $
        int  address = 0x70;                                                            // Address $
        unsigned char buf[10];                                                          // Buffer f$
        if ((fd = open(fileName, O_RDWR)) < 0) {                                        // Open por$
                printf("Cannot open i2c port\n");
                exit(1);
        }


        if (ioctl(fd, I2C_SLAVE_FORCE, address) < 0) {                                  // Set the $
                printf("Unable to get bus access to talk to slave\n");
                exit(1);
        }



        buf[0] = 0;                                                                     // Commands$
        buf[1] = 81;

        if ((write(fd, buf, 2)) != 2) {                                                 // Write co$
                printf("Error writing to i2c slave\n");
                exit(1);
        }

        usleep(70000);                                                                  // this sle$

        buf[0] = 0;                                                                     // This is $

        if ((write(fd, buf, 1)) != 1) {                                                 // Send reg$
                printf("Error writing to i2c slave\n");
                exit(1);
        }

        if (read(fd, buf, 4) != 4) {                                                    // Read bac$
                printf("Unable to read from slave\n");
                exit(1);
        }
        else {
                unsigned char highByte = buf[2];
                unsigned char lowByte = buf[3];
                unsigned int result = (highByte << 8) + lowByte;                        // Calculat$
                //printf("Software v: %u \n", buf[0]);
                printf("Range was: %u\n", result);

        }



        //usleep(1000000);                                                                //Wait fo$



    return EXIT_SUCCESS;
}
