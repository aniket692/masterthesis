#!/usr/bin/python2

import sys
import redpitaya_scpi as scpi
import scipy.fftpack as fftpack
import matplotlib.pyplot as plot
from time import sleep
import matplotlib
matplotlib.use('TkAgg')
import numpy as np
from scipy import stats
from scipy import signal
import subprocess
from subprocess import call
import pywt
from Tkinter import *
import ttk as tk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import tkMessageBox as messagebox

fig = plot.figure(1, figsize=(20, 10))

class App_gui:

	def __init__(self, window):

		self.l_frame=Frame(window, width= 200, height=800, borderwidth=5, relief="raised" )
		self.l_frame.pack(side = "left")
		self.l_frame.grid_propagate(0)

		self.r_frame=Frame(window, width=1100 , height=800, borderwidth=5, relief="sunken")
		self.r_frame.pack(side="right")
		self.r_frame.grid_propagate(0)

		self.input_label = Label(self.l_frame, text = "Decimation")
		self.input_label.grid(row = 0, column=0, columnspan=2, padx = 1, pady = 1)

		self.cmb= tk.Combobox(self.l_frame, values=["1", "8", "64", "1024","8192", "65536"])
		self.cmb.grid(row = 1, column=0, columnspan=2, padx = 1, pady = 1, sticky = "ew")
		self.cmb.current(2)

		self.separator = tk.Separator(self.l_frame, orient= HORIZONTAL)
		self.separator.grid(row = 3, column=0, columnspan=2, padx = 1, pady = 1, sticky = "ew")

		self.input_label2 = Label(self.l_frame, text = "Sampling Range")
		self.input_label2.grid(row = 4, column=0, columnspan=2, padx = 1, pady = 1)

		self.input_field2= Entry(self.l_frame, width="10")
		self.input_field2.grid(row = 5, column=0, columnspan=1,padx = 1, pady = 1, sticky = "ew")
		self.input_field2.insert(END, '12000')
		self.input_field2.focus_set()

		self.input_field3= Entry(self.l_frame, width="10")
		self.input_field3.grid(row = 5, column=1, columnspan=1, padx = 1, pady = 1, sticky = "ew")
		self.input_field3.insert(END, '16384')
		self.input_field3.focus_set()

		self.ok= Button(self.l_frame, text="OK", command = self.btn_click)
		self.ok.grid(row = 6, column=0, columnspan=2, padx = 1, pady = 1, sticky = "ew")

		self.separator = tk.Separator(self.l_frame, orient= HORIZONTAL)
		self.separator.grid(row = 7, column=0, columnspan=2, padx = 1, pady = 1, sticky = "ew")

		self.start= Button(self.l_frame, text="Start Ranging", command = self.start_program)
		self.start.grid(row = 8, column=0, columnspan=2, padx = 1, pady = 1, sticky = "ew")

		self.close= tk.Button(self.l_frame, text="Exit", command= self.btn_close)
		self.close.grid(row = 9, column=0, columnspan=2, padx = 1, pady = 1, sticky = "ew")

		self.separator = tk.Separator(self.l_frame, orient= HORIZONTAL)
		self.separator.grid(row = 10, column=0, columnspan=2, padx = 1, pady = 1, sticky = "ew")

		self.input_label3 = Label(self.l_frame, text = "Measured Range")
		self.input_label3.grid(row = 11 , column=0, columnspan=2, padx = 1, pady = 1)

		self.input_range = StringVar()
		self.input_field4 = Entry(self.l_frame, width="20")
		self.input_field4.grid(row = 12, column=0, columnspan=2, padx = 1, pady = 1)

		self.separator = tk.Separator(self.l_frame, orient= HORIZONTAL)
		self.separator.grid(row = 13, column=0, columnspan=2, padx = 1, pady = 1, sticky = "ew")

		self.input_label5 = Label(self.l_frame, text = "Power of peak frequencies")
		self.input_label5.grid(row = 14 , column=0, columnspan=2, padx = 1, pady = 1, sticky = "ew")

		self.input_label5 = Label(self.l_frame, text = "1. Peak")
		self.input_label5.grid(row = 15 , column=0, columnspan=1, padx = 1, pady = 1, sticky = "ew")

		self.input_peak = StringVar()
		self.input_field5 = Entry(self.l_frame,width="10")
		self.input_field5.grid(row = 15, column=1, columnspan=1, padx = 1, pady = 1, sticky = "ew")

		self.input_label6 = Label(self.l_frame, text = "Max Peak")
		self.input_label6.grid(row = 16 , column=0, columnspan=1, padx = 1, pady = 1, sticky = "ew")

		self.input_maxpeak = StringVar()
		self.input_field6 = Entry(self.l_frame,width="10")
		self.input_field6.grid(row = 16, column=1, columnspan=1, padx = 1, pady = 1, sticky = "ew")

		self.input_label7 = Label(self.l_frame, text = "Frequency of max peak: ")
		self.input_label7.grid(row = 17 , column=0, columnspan=2, padx = 1, pady = 1, sticky = "ew")

		self.input_peak = StringVar()
		self.input_field7 = Entry(self.l_frame,width="10")
		self.input_field7.grid(row = 18, column=0, columnspan=1, padx = 1, pady = 1, sticky = "ew")

		self.input_label8 = Label(self.l_frame, text = "Hz")
		self.input_label8.grid(row = 18 , column=1, columnspan=1, padx = 1, pady = 1, sticky = "ew")

		self.separator = tk.Separator(self.l_frame, orient= HORIZONTAL)
		self.separator.grid(row = 19, column=0, columnspan=2, padx = 1, pady = 1, sticky = "ew")

		self.input_label9 = Label(self.l_frame, text = "   ")
		self.input_label9.grid(row = 20 , column=0, columnspan=2, padx = 1, pady = 1, sticky = "ew")

		self.input_field8 = Entry(self.l_frame,width="10")
		self.input_field8.grid(row = 21, column=0, columnspan=2, padx = 1, pady = 1, sticky = "ew")

		self.canvas = FigureCanvasTkAgg(fig, master=self.r_frame)
		self.canvas._tkcanvas.pack()


	def btn_click(self):
		t = self.input_field2.get()
		te = self.input_field3.get()
		d = self.cmb.get()
		if te<=t:
			messagebox.showwarning("Warning","End value '"+te+"' should be greater than start value '"+t+"'")

	def btn_close(self):
		window.quit()
		window.destroy()
		self.rp_s.tx_txt('DIG:PIN LED1,0')

	def read_buff(self):

		self.rp_s = scpi.scpi(sys.argv[1])
		d = int(self.cmb.get())
		self.rp_s.tx_txt('ACQ:DEC %d' %d)
		self.rp_s.tx_txt('ACQ:TRIG:LEVEL 0')
		self.rp_s.tx_txt('ACQ:START')
		self.rp_s.tx_txt('ACQ:TRIG EXT_PE')
		call(["./iic_py","args","to","iic_py"])
		self.rang= subprocess.check_output(["./iic_py"], universal_newlines=True)
		while True:
			self.rp_s.tx_txt('ACQ:TRIG:STAT?')
			if self.rp_s.rx_txt() == 'TD':
				break
		self.rp_s.tx_txt('ACQ:SOUR1:DATA?')
		buff_string = self.rp_s.rx_txt()
		buff_string = buff_string.strip('{}\n\r').replace("  ", "").split(',')
		buff = map(float, buff_string)
		return buff

	def sig_fft(self, buff):

		self.buff_fft = fftpack.fft(buff)
		self.power = np.abs(self.buff_fft)				#The power of FFT
		self.sample_freq = fftpack.fftfreq(len(buff), d=0.52e-6)	#the corresponding frequency, decimation = 64 -> 8.389ms
		pos_mask = np.where(self.sample_freq > 0)			#find peak freqency
		self.freqs = self.sample_freq[pos_mask]
		self.peak_freq = self.freqs[self.power[pos_mask].argmax()]
		high_buff_fft = self.buff_fft.copy()				#remove high freq
		high_buff_fft[np.abs(self.sample_freq) > self.peak_freq] = 0
		buff_ifft = fftpack.ifft(high_buff_fft)
		return buff_ifft

	def sig_wavelet(self, buff):

		(ca, cd)= pywt.dwt(buff, 'haar')
		cat = pywt.thresholding.soft(ca, np.std(ca)/2)
		cdt = pywt.thresholding.soft(cd, np.std(cd)/2)
		ts_rec = pywt.idwt(cat, cdt, 'haar')
		return ts_rec

	def sig_snr(self, sig):

		snr_sig = stats.signaltonoise(sig, axis=0, ddof=0)
		sig_real = np.abs(snr_sig)
		snr = 20 * np.log10(sig_real)
		return snr

	def start_program(self):
		print("start")
		threshold = 0.02
		t = int(self.input_field2.get())
		te = int(self.input_field3.get())
		buff = self.read_buff()
		buff= buff[t:te]
#FFT
		buff_ifft = self.sig_fft(buff)
#Wavelet
		ts_rec = self.sig_wavelet(buff)
#SNR
		sig_buff = self.sig_snr(buff)
		sig_buff_fft = self.sig_snr(buff_ifft)
		sig_buff_wl = self.sig_snr(ts_rec)

		snr_fft = sig_buff - sig_buff_fft
		snr_wl = sig_buff - sig_buff_wl

		self.input_field4.delete(0, END)
		self.input_field4.insert(END, ''+self.rang+' cm')
#Objekt detection LED:
		detect = ts_rec > threshold
		if (any(detect) > threshold):
			self.rp_s.tx_txt('DIG:PIN LED1,1')
			self.input_field8.delete(0, END)
			self.input_field8.insert(END, 'Object detected')
		else:
			self.rp_s.tx_txt('DIG:PIN LED1,0')
			self.input_field8.delete(0, END)
			self.input_field8.insert(END, 'No object detected')
#Plot signals

		plot.subplot(2,2,1)
		plot.plot(buff, label='Orig.')
		axes_us = plot.subplot(221)
		axes_us.title.set_text('Ultraschallsignal')
		plot.ylabel('Voltage')
		plot.hold('on')
		plot.plot(buff_ifft, 'g', label= 'FFT. SNR: %.2f dB' % snr_fft)
		plot.legend(loc='upper left')

		plot.subplot(2, 2, 2)
		plot.plot(buff, label='Orig.' )
		axes_us.title.set_text('Ultraschallsignal')
		plot.ylabel('Voltage')
		plot.hold('on')
		plot.plot(ts_rec, 'r', label='Wavelet. SNR: %.2f dB' % snr_wl)
		plot.legend(loc='upper left')
#Realpart
		real_sample_freq= np.abs(self.sample_freq)
		peak = self.peak_freq
		peaks_sample= np.abs(self.buff_fft[0:len(self.buff_fft)/4])

		peaks = signal.argrelextrema(peaks_sample, np.greater, order=10)
		peaked=peaks[0]

		power = peaks_sample[peaks]
		self.input_field6.delete(0, END)
		self.input_field6.insert(END, int(np.amax(power)))

		self.input_field5.delete(0, END)
		self.input_field5.insert(END, int(power[0]))

		self.input_field7.delete(0, END)
		self.input_field7.insert(END, int(peak))

		plot.subplot(2, 2, 4)
		plot.plot(peaks_sample)
		plot.scatter(peaks, self.power[peaks],linewidth=0.3, s=50, c='r')
		plot.title('Peak frequency')
		plot.xlim(0, 200)
		plot.ylabel('Power')


		plot.subplot(2, 2, 3)
		plot.plot(real_sample_freq, self.power)
		plot.scatter(peak, np.amax(power), color='r', s=40)
		axes_us.title.set_text('Peak frequencies')
		plot.xlim(0, peak+1000000)
		plot.xlabel('Frequency [Hz]')
		plot.ylabel('Power')

		self.canvas.draw()
		fig.clear()
		sleep(1)
		print("ende")


window = Tk()
window.title("Projekt")
window.geometry('1300x800')
window.resizable(0, 0)

app_gui= App_gui(window)
window.mainloop()
